<?php

namespace HalcyonLaravel\History\Tests\Models\Auth;

use HalcyonLaravel\Base\Models\Traits\ModelTraits;
use HalcyonLaravel\History\Models\Contracts\UserHistoryTrait;
use HalcyonLaravel\History\Traits\Historable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements UserHistoryTrait
{
    use HasRoles,
        Notifiable,
        Historable,
        ModelTraits;

    protected $fillable = [
        'first_name',
        'last_name',
        'status',
        'slug',
    ];

    /**
     * Return the baseable name for this model.
     *
     * @return String
     */
    public function baseable(): array
    {
        return [
            'source' => 'first_name',
        ];
    }

    public function historableName(): string
    {
        return $this->first_name;
    }

    /**
     * Return the links related to this model.
     *
     * @return array
     */
    public function links(): array
    {
        return [

            // 'frontend' => [

            //     'show' => []

            // ]

        ];
    }
}
