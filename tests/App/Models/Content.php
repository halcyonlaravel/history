<?php

namespace HalcyonLaravel\History\Tests\Models;

use HalcyonLaravel\History\Traits\Historable;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use Historable;

    protected $fillable = [
        'name',
        'slug',
        'content',
        'description',
        'status',
        'image',
        'category_id'
    ];

    /**
     * Return the baseable name for this model.
     *
     * @return array
     */
    public function baseable(): array
    {
        return [
            'source' => 'name'
        ];
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Return the action links for the model.
     *
     * @param bool $frontend
     * @return array
     */
    public function actions($frontend = false)
    {
        $user = auth()->user();
        $actions = [];

        // Frontend Links
        if ($frontend) {
            $actions['show'] = ['type' => 'show', 'link' => route('frontend.content.show', $this)];
            return $actions;
        }

        // Backend Links
        if ($this->trashed()) {
            if ($user->can('content restore')) {
                $actions['restore'] = [
                    'type' => 'restore',
                    'link' => route('admin.content.restore', $this),
                    'redirect' => route('admin.content.index')
                ];
            }
            if ($user->can('content purge')) {
                $actions['purge'] = [
                    'type' => 'purge',
                    'link' => route('admin.content.purge', $this),
                    'redirect' => route('admin.content.index')
                ];
            }

            return $actions;
        }


        if ($user->can('content show')) {
            $actions['show'] = ['type' => 'show', 'link' => route('admin.content.show', $this)];
        }
        if ($user->can('content update')) {
            $actions['edit'] = ['type' => 'edit', 'link' => route('admin.content.edit', $this)];
        }
        if ($user->can('content delete')) {
            $actions['delete'] = [
                'type' => 'delete',
                'link' => route('admin.content.destroy', $this),
                'redirect' => route('admin.content.deleted')
            ];
        }

        return $actions;
    }
}
