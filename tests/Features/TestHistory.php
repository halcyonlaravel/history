<?php

namespace HalcyonLaravel\History\Tests\Features;

use HalcyonLaravel\History\Models\History as HistoryModel;
use HalcyonLaravel\History\Tests\TestCase;
use History;

class TestHistory extends TestCase
{
    public function setUp():void
    {
        parent::setUp();
        HistoryModel::query()->truncate();
    }

    public function testCreateUpdate()
    {
        $this->actingAs($this->admin);

        History::created($this->user);

        $expectedCreated = [
            'user_id' => $this->admin->id,
            'historable_id' => $this->user->id,
            'historable_type' => get_class($this->user),
            'type' => 'created',
            'message' => null,
        ];
        $this->assertDatabaseHas((new HistoryModel)->getTable(), $expectedCreated);
    }

    public function testUpdated()
    {
        $this->actingAs($this->admin);

        $oldModel = $this->user->getOriginal();
        $this->user->update([
            'first_name' => 'New Name',
            'last_name' => 'New Last name',
        ]);

        History::updated($this->user, $oldModel);

        $expectedUpdated = [
            'user_id' => $this->admin->id,
            'historable_id' => $this->user->id,
            'historable_type' => get_class($this->user),
            'type' => 'updated',
            'message' => null,

            'changes' => "[{\"field\":\"first_name\",\"data\":{\"new\":\"New Name\",\"old\":\"Basic\"}},{\"field\":\"last_name\",\"data\":{\"new\":\"New Last name\",\"old\":\"User\"}}]",
        ];
        $this->assertDatabaseHas((new HistoryModel)->getTable(), $expectedUpdated);
    }

    public function testDeleted()
    {
        $this->actingAs($this->admin);
        History::deleted($this->user);

        $expectedDeleted = [
            'user_id' => $this->admin->id,
            'historable_id' => $this->user->id,
            'historable_type' => get_class($this->user),
            'type' => 'deleted',
            'changes' => null,
            'message' => $this->user->base(''),

        ];
        $this->assertDatabaseHas((new HistoryModel)->getTable(), $expectedDeleted);
    }

    public function testPurged()
    {
        $this->actingAs($this->admin);
        History::purged($this->user);

        $expectedPurge = [
            'user_id' => $this->admin->id,
            'historable_id' => $this->user->id,
            'historable_type' => get_class($this->user),
            'type' => 'purged',
            'changes' => null,
            'message' => $this->user->base(''),
        ];
        $this->assertDatabaseHas((new HistoryModel)->getTable(), $expectedPurge);
    }

    public function testRestored()
    {
        $this->actingAs($this->admin);
        History::restored($this->user);

        $expectedRestored = [
            'user_id' => $this->admin->id,
            'historable_id' => $this->user->id,
            'historable_type' => get_class($this->user),
            'type' => 'restored',
            'changes' => null,
            'message' => null,
        ];
        $this->assertDatabaseHas((new HistoryModel)->getTable(), $expectedRestored);
    }
}
