<?php

namespace HalcyonLaravel\History\Tests\Features;

use HalcyonLaravel\History\Models\History as HistoryModel;
use HalcyonLaravel\History\Tests\TestCase;
use History;

class TestGuest extends TestCase
{
    public function setUp():void
    {
        parent::setUp();
        HistoryModel::query()->truncate();
    }

    public function testGuest()
    {

        History::created($this->user);

        $expectedCreated = [
            'user_id' => null,
            'historable_id' => $this->user->id,
            'historable_type' => get_class($this->user),
            'type' => 'created',
            'message' => null,
            'changes' => null,
        ];
        $this->assertDatabaseHas((new HistoryModel)->getTable(), $expectedCreated);

    }
}
