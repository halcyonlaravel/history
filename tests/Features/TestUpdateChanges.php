<?php

namespace HalcyonLaravel\History\Tests\Features;

use HalcyonLaravel\History\Models\History as HistoryModel;
use HalcyonLaravel\History\Tests\TestCase;
use History;

class TestUpdateChanges extends TestCase
{
    public function setUp():void
    {
        parent::setUp();
        HistoryModel::query()->truncate();
    }

    public function testUpdatedTwoColumns()
    {
        $this->actingAs($this->admin);

        $oldModel = $this->user->getOriginal();
        $this->user->update([
            'first_name' => 'New Name',
            'last_name' => 'New Last name',
        ]);

        History::updated($this->user, $oldModel);

        $changes = [
            [
                'field' => 'first_name',
                'data' => [
                    'new' => $this->user->first_name,
                    'old' => $oldModel['first_name'],
                ],
            ],
            [
                'field' => 'last_name',
                'data' => [
                    'new' => $this->user->last_name,
                    'old' => $oldModel['last_name'],
                ],
            ],
        ];
        $expectedUpdated = [
            'user_id' => $this->admin->id,
            'historable_id' => $this->user->id,
            'historable_type' => get_class($this->user),
            'type' => 'updated',
            'message' => null,
            'changes' => json_encode($changes),
        ];
        $this->assertDatabaseHas((new HistoryModel)->getTable(), $expectedUpdated);
    }

    public function testUpdatedOneColumn()
    {
        $this->actingAs($this->admin);

        $oldModel = $this->user->getOriginal();
        $this->user->update([
            'first_name' => 'New Name',
        ]);

        History::updated($this->user, $oldModel);

        $changes = [
            [
                'field' => 'first_name',
                'data' => [
                    'new' => $this->user->first_name,
                    'old' => $oldModel['first_name'],
                ],
            ],
        ];

        $expectedUpdated = [
            'user_id' => $this->admin->id,
            'historable_id' => $this->user->id,
            'historable_type' => get_class($this->user),
            'type' => 'updated',
            'message' => null,
            'changes' => json_encode($changes),
        ];
        $this->assertDatabaseHas((new HistoryModel)->getTable(), $expectedUpdated);
    }
}
