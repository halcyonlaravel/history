<?php

namespace HalcyonLaravel\History\Tests\Features;

use HalcyonLaravel\History\Models\History as HistoryModel;
use HalcyonLaravel\History\Tests\TestCase;
use History;

class TestUser extends TestCase
{
    public function setUp():void
    {
        parent::setUp();
        HistoryModel::query()->truncate();
    }

    public function testGetUser()
    {
        $this->actingAs($this->admin);

        History::created($this->user);

        $expectedCreated = [
            'user_id' => $this->admin->id,
            'historable_id' => $this->user->id,
            'historable_type' => get_class($this->user),
            'type' => 'created',
            'message' => null,
            'changes' => null,
        ];
        $this->assertDatabaseHas((new HistoryModel)->getTable(), $expectedCreated);

        $history = HistoryModel::first();

        // Here will then the config
        $this->assertEquals($history->user->id, $this->admin->id);
        $this->assertEquals($history->user->first_name, $this->admin->first_name);
    }
}
