<?php

namespace HalcyonLaravel\History\Tests\Features;

use HalcyonLaravel\History\Models\History as HistoryModel;
use HalcyonLaravel\History\Tests\Models\Auth\User;
use HalcyonLaravel\History\Tests\TestCase;
use History;

class TestList extends TestCase
{
    public function setUp():void
    {
        parent::setUp();
        HistoryModel::query()->truncate();
    }

    public function testRenderTypeNoData()
    {
        $this->markTestSkipped(
            'testRenderTypeNoData not yet done'
        );
        $this->assertEquals('There are no recent activities found.', History::renderType(User::class));
    }

    // public function testRenderType()
    // {
    //     $this->actingAs($this->admin);

    //     History::created($this->user);
    //     History::created($this->user);

    //     $this->assertEquals('There are no recent activities found.', History::renderType(User::class));
    // }
}
