<?php

namespace HalcyonLaravel\History\Tests;

use HalcyonLaravel\History\Tests\Models\Auth\User;
use HalcyonLaravel\History\Tests\Models\Content;
use Illuminate\Database\Schema\Blueprint;
use Orchestra\Testbench\TestCase as Orchestra;
use Route;

// use Spatie\Permission\Contracts\Role;

class TestCase extends Orchestra
{
    protected $user;
    protected $admin;
    protected $content;

    public function setUp():void
    {
        parent::setUp();
        $this->setUpDatabase($this->app);
        $this->setUpSeed();
        $this->setUpRoutes();
        config([
            'halcyon-laravel.history' => [
                'auth' => [
                    'user' => [
                        /**
                         * This will affect getting user whos created history.
                         */
                        'model' => 'HalcyonLaravel\History\Tests\Models\Auth\User',
                    ]
                ],
                'baseableName' => 'historyName',

                // Spatie permission master role
                'master_role_name' => 'system',
            ],
            'formats' => [
//        'date' => 'F jS, Y',
                'time_12' => 'g:ia',
//        'time_24' => 'g:i',
//        'datetime_12' => 'F jS, Y g:ia',
//        'datetime_24' => 'F jS, Y g:i',
            ],

            'routes' => [
                'user' => [
                    'show' => 'admin.auth.user.show',
                ],
            ],
        ]);
    }

    /**
     * Set up the database.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    protected function setUpDatabase($app)
    {
        // test Migrations
        $app['db']->connection()->getSchemaBuilder()->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
        });

        $app['db']->connection()->getSchemaBuilder()->create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            // $table->string('slug');
            $table->text('content');
            $table->text('description')->nullable();
            $table->enum('status', ['active', 'inactive']);
            $table->text('image')->nullable();
            $table->integer('category_id')->nullable(); // Temp

            $table->timestamps();
            // $table->softDeletes();
        });

        include_once __DIR__ . '/../vendor/spatie/laravel-permission/database/migrations/create_permission_tables.php.stub';
        (new \CreatePermissionTables())->up();

        // Main Migration
        $app['db']->connection()->getSchemaBuilder()->create('histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('historable_id');
            $table->string('historable_type');
            $table->enum('type', [
                'created',
                'deleted',
                'updated',
                'restored',
                'purged'
            ]);
            $table->text('changes')->nullable(); // soon to json
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    protected function setUpSeed()
    {
        $this->admin = User::create([
            'first_name' => 'Istrator',
            'last_name' => 'Admin',
        ]);

        $this->user = User::create([
            'first_name' => 'Basic',
            'last_name' => 'User',
        ]);

        $this->content = Content::create([
            'name' => 'Content Name',
            'content' => 'Content content',
            'description' => 'Content content',
            'image' => 'http://test.com/me.png',
            'status' => 'active',
        ]);
    }

    protected function setUpRoutes()
    {
        Route::get('admin/user/{user}', ['as' => 'admin.user.show',]);
        Route::get('admin/auth/user/{user}', ['as' => 'admin.auth.user.show',]);
    }

    public function tearDown():void
    {
        parent::tearDown();
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }


    protected function getPackageAliases($app)
    {
        return [
            "History" => "HalcyonLaravel\\History\\Facades\\History",
        ];
    }

    protected function getPackageProviders($app)
    {
        return [
            "HalcyonLaravel\\Base\\Providers\\BaseServiceProvider",
            "HalcyonLaravel\\History\\Providers\\HistoryServiceProvider",

            "Spatie\\Permission\\PermissionServiceProvider",
        ];
    }
}
