<?php

namespace HalcyonLaravel\History;

/**
 * Class History.
 */
class History
{
    use HistoryHelper, HistoryRenderer;

    private $_loggedInUser;
    private $_isNotMasterRole;

    public function __construct()
    {
        if (auth()->check()) {
            $this->_loggedInUser = auth()->user();
            $this->_isNotMasterRole = !$this->_loggedInUser->hasRole(config('halcyon-laravel.history.master_role_name'));
        }
    }
}
