<?php

namespace HalcyonLaravel\History\Models\Contracts;

interface UserHistoryTrait
{
    public function historableName(): string;
}
