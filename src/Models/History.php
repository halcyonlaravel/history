<?php

namespace HalcyonLaravel\History\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $fillable = [
        'user_id',
        'historable_type',
        'historable_id',
        'type',
        'changes',
        'message'
    ];

    public function historable()
    {
        if (method_exists(app($this->historable_type), 'bootSoftDeletes')) {
            return $this->morphTo()->withTrashed();
        } else {
            return $this->morphTo();
        }
    }

    public function user()
    {
        return $this->belongsTo(config('halcyon-laravel.history.auth.user.model'));
    }

    public function getName()
    {
        $classModel = ('\\' . $this->historable_type);

        if (method_exists($classModel, 'bootSoftDeletes')) {
            $model = ('\\' . $this->historable_type)::withTrashed()->find($this->historable_id);
        } else {
            $model = ('\\' . $this->historable_type)::find($this->historable_id);
        }
        if (is_null($model)) {
            return $this->message;
        }
        return $model->base(config('halcyon-laravel.history.baseableName'));
    }
}
