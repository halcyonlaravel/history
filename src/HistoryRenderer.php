<?php

namespace HalcyonLaravel\History;

use HalcyonLaravel\History\Models\History as Model;
use Illuminate\Database\Eloquent\Model as BaseModel;

trait HistoryRenderer
{
    private $histories;

    public function buildEntity(BaseModel $historable)
    {
        $this->histories = $this->_validated()
            ->where('historable_type', get_class($historable))
            ->where('historable_id', $historable->id);
        return $this;
    }

    private function _validated()
    {
        if ($this->_isNotMasterRole) {
            return $this->_userBuild();
        }

        return $this->_build();
    }

    private function _userBuild($userID = null)
    {
        $userID = $userID ?? $this->_loggedInUser->id;
        return $this->_build()
            ->whereHas('user', function ($q) use ($userID) {
                $q->where('id', $userID);
            });
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Builder
     */
    public function _build()
    {
        return Model::with(['user', 'historable']);
    }

    public function buildClass(string $class)
    {
        $this->histories = $this->_validated()
            ->where('historable_type', $class);
        return $this;
    }

    public function buildUser($userID = null)
    {
        $this->histories = $this->_userBuild($userID);
        return $this;
    }

    public function getHistories()
    {
        return $this->histories;
    }

    /**
     * @param null $paginate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function render($paginate = null)
    {
        if (is_null($this->histories)) {
            throw new \Exception('No query selected for History Model');
        }
        $histories = $this->histories->whereHas('user')->latest();
        if (!is_null($paginate)) {
            $histories = $histories->paginate($paginate);
        } else {
            $histories = $histories->get();
        }
        return view('history::list', compact('histories', 'paginate'));
    }
}
