<article class="timeline-entry">
    <div class="timeline-entry-inner">
        <div class="timeline-icon bg-primary">
            {{-- <i class="fa fa-check"></i> --}}
        </div>
        <div class="timeline-label">
            <time class="clearfix">
                @if(auth()->guest())
                    <span>{{ $history->updated_at->format(config('halcyon-laravel.history.formats.time_12')) }}</span>
                    <span>{{ $history->created_at->diffForHumans() }}</span>
                @else
                    <span>{{ $history->updated_at->timezone(app('auth')->user()->timezone)->format(config('halcyon-laravel.history.formats.time_12')) }}</span>
                    <span>{{ $history->created_at->timezone(app('auth')->user()->timezone)->diffForHumans() }}</span>
                @endif
            </time>
            <p>{!!
				__('history::message.actions.' . $history->type, 
					['name' => $history->historable ? $history->historable->base(config('halcyon-laravel.history.baseableName')) : $history->getName(), 
					'user' => '<b><a href="'. route(config('halcyon-laravel.history.routes.user.show'), $history->user) .'">'.$history->user->name.'</a> <span></span></b>'])
			!!}</p>
        </div>
    </div>

</article>
