<?php

namespace HalcyonLaravel\History\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ContractException extends HttpException
{
    public static function required(): self
    {
        return new static(402, trans('history::exceptions.contract_required'), null, []);
    }
}
