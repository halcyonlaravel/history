<?php

namespace HalcyonLaravel\History;

use HalcyonLaravel\History\Models\History as Model;
use Illuminate\Database\Eloquent\Model as BaseModel;

trait HistoryHelper
{
    public function log(BaseModel $model, $userOrID = null, $data)
    {
        $data['user_id'] = $this->_getUserID($userOrID);
        return $this->_log($model, $data);
    }

    private function _getUserID($userOrID)
    {
        if ($userOrID instanceof BaseModel) {
            return $userOrID->id;
        }
        return $userOrID ?: (empty($this->_loggedInUser) ? null : $this->_loggedInUser->id);
    }

    private function _log(BaseModel $model, $data): Model
    {
        $data['historable_id'] = $model->id;
        $data['historable_type'] = get_class($model);
        return Model::create($data);
    }

    /**
     * History Log Create Template
     *
     * @param BaseModel $model
     * @param null      $userOrID
     * @param array     $data
     *
     * @return Model
     */
    public function created(BaseModel $model, $userOrID = null, $data = [])
    {
        $data['type'] = 'created';
        $data['user_id'] = $this->_getUserID($userOrID);
        return $this->_log($model, $data);
    }

    /**
     * History Log Create Template
     *
     * @param BaseModel $model
     * @param null      $oldModel
     * @param null      $userOrID
     * @param array     $data
     *
     * @return Model
     */
    public function updated(BaseModel $model, $oldModel = null, $userOrID = null, $data = [])
    {
        $data['type'] = 'updated';
        $data['user_id'] = $this->_getUserID($userOrID);
        $data['changes'] = $this->_changes($model, $oldModel);
        return $this->_log($model, $data);
    }

    /**
     * @param      $model
     * @param null $oldModel
     *
     * @return false|string|null
     */
    private function _changes($model, $oldModel = null)
    {
        if (is_null($oldModel)) {
            return null;
        }

        $oldModel = is_object($oldModel) ? $oldModel : (object)$oldModel;
        $row = function ($field, $new, $old) {
            return [
                'field' => $field,
                'data' => [
                    'new' => $new,
                    'old' => $old,
                ],
            ];
        };

        $changes = [];
        foreach ($oldModel as $field => $value) {
            if ($model->$field != $oldModel->$field) {
                $changes[] = $row($field, $model->$field, $oldModel->$field);
            }
        }

        return json_encode($changes);
    }

    /**
     * History Log Delete Template
     *
     * @param BaseModel $model
     * @param null      $userOrID
     * @param array     $data
     *
     * @return Model
     */
    public function deleted(BaseModel $model, $userOrID = null, $data = [])
    {
        $data['type'] = 'deleted';
        $data['user_id'] = $this->_getUserID($userOrID);
        $data['message'] = $model->base(config('halcyon-laravel.history.baseableName'));
        return $this->_log($model, $data);
    }

    /**
     * History Log Restore Template
     *
     * @param BaseModel $model
     * @param null      $userOrID
     * @param array     $data
     *
     * @return Model
     */
    public function restored(BaseModel $model, $userOrID = null, $data = [])
    {
        $data['type'] = 'restored';
        $data['user_id'] = $this->_getUserID($userOrID);
        return $this->_log($model, $data);
    }

    /**
     * History Log Purge Template
     *
     * @param BaseModel $model
     * @param null      $userOrID
     * @param array     $data
     *
     * @return Model
     */
    public function purged(BaseModel $model, $userOrID = null, $data = [])
    {
        $data['type'] = 'purged';
        $data['user_id'] = $this->_getUserID($userOrID);
        $data['message'] = $model->base(config('halcyon-laravel.history.baseableName'));
        return $this->_log($model, $data);
    }
}
