<?php

return [
    'contract_required' => 'One of instance must required for model either "HalcyonLaravel\History\Models\Contracts\UserHistoryTrait" or "HalcyonLaravel\Base\Models\Model"',
];
