<?php

namespace HalcyonLaravel\History\Providers;

use HalcyonLaravel\History\History;
use Illuminate\Support\ServiceProvider;

class HistoryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/halcyon-laravel/history.php' => config_path('halcyon-laravel/history.php'),
        ]);

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'history');
    }

    public function register()
    {
        $this->app->bind('history', function ($app) {
            return new History($app);
        });


        $this->loadMigrationsFrom(__DIR__ . '/../migrations');
        $this->loadViewsFrom(__DIR__ . '/../views', 'history');

        $this->mergeConfigFrom(
            __DIR__ . '/../config/halcyon-laravel/history.php',
            'halcyon-laravel.history'
        );
    }
}
