<?php

namespace HalcyonLaravel\History\Traits;

use HalcyonLaravel\History\Models\History as Model;

trait Historable
{
    public function histories()
    {
        return $this->morphMany(Model::class, 'historable');
    }

    public function author()
    {
        return $this->historable()->where('action', 'create')->firstOrFail()->user();
    }
}
