# Halcyon Packages History - v1.0.12 #

## Required  ##
```
"halcyon-laravel/base": ">=1"
```
### Changes Log ###
- v1.1.2: 2021-09-15 Add support for permission v5
- v1.1.1: 2021-08-16 Add support for permission v4
- v1.1.0: 2019-09-26 Add support for laravel 6
- v1.0.12: 2019-02-02 fix guest
- v1.0.11: 2019-01-22 add config for user route
- v1.0.10: 2018-12-04 Fix multi definition in test, PSR-1 PSR-2
- v1.0.9: 2018-12-03 Compatible with base v2
- v1.0.8: 2018-08-31 Add lang 'es'
- v1.0.7: 2018-05-30 Fix get user when seeding
- v1.0.6: 2018-05-30 fix saving auth id, then now can see only own history, except role on config
- v1.0.5: 2018-05-29 version tag
- v1.0.4: 2018-05-28 sorting
- v1.0.3: 2018-04-26 fix for delete
- v1.0.2: 2018-04-26
- v1.0.1: 2018-04-26
- v1.0.0: 2018-04-26